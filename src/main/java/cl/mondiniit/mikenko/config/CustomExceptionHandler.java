package cl.mondiniit.mikenko.config;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import cl.mondiniit.mikenko.exception.UserNotFoundException;
import cl.mondiniit.mikenko.to.ResponseTO;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {

		ResponseTO responseTO = ResponseTO.getDefault();
		return handleExceptionInternal(ex, responseTO, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	@ExceptionHandler(value = UserNotFoundException.class)
	protected ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) {

		ResponseTO responseTO = ResponseTO.getDefault();
		return handleExceptionInternal(ex, responseTO, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}

}