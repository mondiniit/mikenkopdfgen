package cl.mondiniit.mikenko.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.mondiniit.mikenko.service.FileService;

@RestController
public class ParamsController {

	@Autowired
	private FileService fileService;
	
	@GetMapping("/params/file-types")
	public List<String> getFileTypes() {
		return fileService.getFileTypes();
	}

}
