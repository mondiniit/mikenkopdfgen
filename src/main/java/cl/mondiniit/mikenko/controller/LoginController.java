package cl.mondiniit.mikenko.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    @GetMapping("/")
    public String redirectToLogin(){
        return "/login";
    }
    @GetMapping("/login")
    public String loginForm(){
        return "";
    }

    @PostMapping("/logincheck")
    public String loginCheck(){
        return "";
    }
}
