package cl.mondiniit.mikenko.controller;

import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cl.mondiniit.mikenko.exception.UserNotFoundException;
import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.repo.MeasureRepo;
import cl.mondiniit.mikenko.repo.UserRepo;
import cl.mondiniit.mikenko.service.FileService;
import cl.mondiniit.mikenko.service.MeasureService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private MeasureService measureService;

	@Autowired
	private FileService fileService;

	@Autowired
	private MeasureRepo measureRepo;

	@GetMapping("/getuser")
	public String getUser() {
		return "";
	}

	@PostMapping("/setuser")
	public String setUser() {
		return "";
	}

	@GetMapping("/{user}/measures")
	public List<Measure> getMeasures(@PathVariable String user, @RequestParam(required = false) String fileType)
			throws UserNotFoundException {

		return measureService.getMeasuresByUser(user, fileType);
	}

	@PostMapping("/measures")
	public String addMeasures(@RequestParam MultipartFile file, @RequestParam String fileType)
			throws IOException {
		return measureService.addMeasure(file, fileType);
	}

	/* Falta agregar peso e altura */

}
