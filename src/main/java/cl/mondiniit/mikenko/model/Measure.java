package cl.mondiniit.mikenko.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Measure {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private BigDecimal id;

	private String nombre;

	private String rut;

	private String edad;
	private String sexo;
	private String fono;
	private String medico;
	private String ingreso;
	private String informe;
	private String numeroOrden;

	private String creatinina;
	private String tasaDeFiltracionGlomedularEstimada;
	private String valorDeRefereniaTFGEstimada;

	private String tiroEstimulante;
	private String t4Libre;

	private String hemoglobina;
	private String glicemiaPromedio;

	private String glucosa;
	private String insulina;
	private String resistenciaInsulina;

	private String sodio;
	private String potasio;
	private String cloro;

	private String vitamina;

	private String colesterolTotal;
	private String colesterolHdl;
	private String colesterolVldl;
	private String colesterolLdl;
	private String trigliceridos;
	private String factorDeRiesgoCardiovascular;
	private String metodo;
	private String tecnologiaTipoMuestra;

	private String glicemia;
	private String nitrogenoUrico;
	private String uremia;
	private String colesterol;
	private String acidoUrico;
	private String proteinasTotales;
	private String albumina;
	private String calcio;
	private String fosforo;
	private String bilirrubina;
	private String fosfatasa;
	private String deshidrogenasaLactica;
	private String transaminasa;

	private String fileType;

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getFono() {
		return fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	public String getMedico() {
		return medico;
	}

	public void setMedico(String medico) {
		this.medico = medico;
	}

	public String getIngreso() {
		return ingreso;
	}

	public void setIngreso(String ingreso) {
		this.ingreso = ingreso;
	}

	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	public String getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	public String getCreatinina() {
		return creatinina;
	}

	public void setCreatinina(String creatinina) {
		this.creatinina = creatinina;
	}

	public String getTasaDeFiltracionGlomedularEstimada() {
		return tasaDeFiltracionGlomedularEstimada;
	}

	public void setTasaDeFiltracionGlomedularEstimada(String tasaDeFiltracionGlomedularEstimada) {
		this.tasaDeFiltracionGlomedularEstimada = tasaDeFiltracionGlomedularEstimada;
	}

	public String getValorDeRefereniaTFGEstimada() {
		return valorDeRefereniaTFGEstimada;
	}

	public void setValorDeRefereniaTFGEstimada(String valorDeRefereniaTFGEstimada) {
		this.valorDeRefereniaTFGEstimada = valorDeRefereniaTFGEstimada;
	}

	public String getTiroEstimulante() {
		return tiroEstimulante;
	}

	public void setTiroEstimulante(String tiroEstimulante) {
		this.tiroEstimulante = tiroEstimulante;
	}

	public String getT4Libre() {
		return t4Libre;
	}

	public void setT4Libre(String t4Libre) {
		this.t4Libre = t4Libre;
	}

	public String getHemoglobina() {
		return hemoglobina;
	}

	public void setHemoglobina(String hemoglobina) {
		this.hemoglobina = hemoglobina;
	}

	public String getGlicemiaPromedio() {
		return glicemiaPromedio;
	}

	public void setGlicemiaPromedio(String glicemiaPromedio) {
		this.glicemiaPromedio = glicemiaPromedio;
	}

	public String getGlucosa() {
		return glucosa;
	}

	public void setGlucosa(String glucosa) {
		this.glucosa = glucosa;
	}

	public String getInsulina() {
		return insulina;
	}

	public void setInsulina(String insulina) {
		this.insulina = insulina;
	}

	public String getResistenciaInsulina() {
		return resistenciaInsulina;
	}

	public void setResistenciaInsulina(String resistenciaInsulina) {
		this.resistenciaInsulina = resistenciaInsulina;
	}

	public String getSodio() {
		return sodio;
	}

	public void setSodio(String sodio) {
		this.sodio = sodio;
	}

	public String getPotasio() {
		return potasio;
	}

	public void setPotasio(String potasio) {
		this.potasio = potasio;
	}

	public String getCloro() {
		return cloro;
	}

	public void setCloro(String cloro) {
		this.cloro = cloro;
	}

	public String getVitamina() {
		return vitamina;
	}

	public void setVitamina(String vitamina) {
		this.vitamina = vitamina;
	}

	public String getColesterolTotal() {
		return colesterolTotal;
	}

	public void setColesterolTotal(String colesterolTotal) {
		this.colesterolTotal = colesterolTotal;
	}

	public String getColesterolHdl() {
		return colesterolHdl;
	}

	public void setColesterolHdl(String colesterolHdl) {
		this.colesterolHdl = colesterolHdl;
	}

	public String getColesterolVldl() {
		return colesterolVldl;
	}

	public void setColesterolVldl(String colesterolVldl) {
		this.colesterolVldl = colesterolVldl;
	}

	public String getColesterolLdl() {
		return colesterolLdl;
	}

	public void setColesterolLdl(String colesterolLdl) {
		this.colesterolLdl = colesterolLdl;
	}

	public String getTrigliceridos() {
		return trigliceridos;
	}

	public void setTrigliceridos(String trigliceridos) {
		this.trigliceridos = trigliceridos;
	}

	public String getFactorDeRiesgoCardiovascular() {
		return factorDeRiesgoCardiovascular;
	}

	public void setFactorDeRiesgoCardiovascular(String factorDeRiesgoCardiovascular) {
		this.factorDeRiesgoCardiovascular = factorDeRiesgoCardiovascular;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getTecnologiaTipoMuestra() {
		return tecnologiaTipoMuestra;
	}

	public void setTecnologiaTipoMuestra(String tecnologiaTipoMuestra) {
		this.tecnologiaTipoMuestra = tecnologiaTipoMuestra;
	}

	public String getGlicemia() {
		return glicemia;
	}

	public void setGlicemia(String glicemia) {
		this.glicemia = glicemia;
	}

	public String getNitrogenoUrico() {
		return nitrogenoUrico;
	}

	public void setNitrogenoUrico(String nitrogenoUrico) {
		this.nitrogenoUrico = nitrogenoUrico;
	}

	public String getUremia() {
		return uremia;
	}

	public void setUremia(String uremia) {
		this.uremia = uremia;
	}

	public String getColesterol() {
		return colesterol;
	}

	public void setColesterol(String colesterol) {
		this.colesterol = colesterol;
	}

	public String getAcidoUrico() {
		return acidoUrico;
	}

	public void setAcidoUrico(String acidoUrico) {
		this.acidoUrico = acidoUrico;
	}

	public String getProteinasTotales() {
		return proteinasTotales;
	}

	public void setProteinasTotales(String proteinasTotales) {
		this.proteinasTotales = proteinasTotales;
	}

	public String getAlbumina() {
		return albumina;
	}

	public void setAlbumina(String albumina) {
		this.albumina = albumina;
	}

	public String getCalcio() {
		return calcio;
	}

	public void setCalcio(String calcio) {
		this.calcio = calcio;
	}

	public String getFosforo() {
		return fosforo;
	}

	public void setFosforo(String fosforo) {
		this.fosforo = fosforo;
	}

	public String getBilirrubina() {
		return bilirrubina;
	}

	public void setBilirrubina(String bilirrubina) {
		this.bilirrubina = bilirrubina;
	}

	public String getFosfatasa() {
		return fosfatasa;
	}

	public void setFosfatasa(String fosfatasa) {
		this.fosfatasa = fosfatasa;
	}

	public String getDeshidrogenasaLactica() {
		return deshidrogenasaLactica;
	}

	public void setDeshidrogenasaLactica(String deshidrogenasaLactica) {
		this.deshidrogenasaLactica = deshidrogenasaLactica;
	}

	public String getTransaminasa() {
		return transaminasa;
	}

	public void setTransaminasa(String transaminasa) {
		this.transaminasa = transaminasa;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Measure() {
	}

}
