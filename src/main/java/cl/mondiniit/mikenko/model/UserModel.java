package cl.mondiniit.mikenko.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity
public class UserModel {

	@Id
	private String rut;
	private String name;
	private String age;
	private String weight;
	private String hight;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Measure> measures;

	public UserModel() {
	}

	public UserModel(String rut, String name, String age, String weight, String hight) {
		super();
		this.rut = rut;
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.hight = hight;
	}

	@Override
	public String toString() {
		return "UserModel [rut=" + rut + ", name=" + name + "]";
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHight() {
		return hight;
	}

	public void setHight(String hight) {
		this.hight = hight;
	}

	public List<Measure> getMeasures() {
		return measures;
	}

	public void setMeasures(List<Measure> measures) {
		this.measures = measures;
	}

}
