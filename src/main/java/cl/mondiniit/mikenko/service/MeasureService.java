package cl.mondiniit.mikenko.service;

import cl.mondiniit.mikenko.exception.UserNotFoundException;
import cl.mondiniit.mikenko.repo.MeasureRepo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.repo.UserRepo;

@Service
public class MeasureService {

  @Autowired
  private UserRepo userRepo;

  @Autowired
  private MeasureRepo measureRepo;

  @Autowired
  private FileService fileService;

  public MeasureService(UserRepo userRepo, FileService fileService, MeasureRepo measureRepo) {
    this.userRepo = userRepo;
    this.fileService = fileService;
    this.measureRepo = measureRepo;
  }

  public String addMeasure(MultipartFile file, String fileType) throws IOException {

    if (!fileService.getFileTypes().contains(fileType)) {
      throw new IllegalArgumentException("The fileType " + fileType + " is not supported");
    }

    // TODO el llamado de lectura va aqui

    String[] lines = fileService.getLines(file);

    Map<String, Object> results = fileService.readByFileType(fileType, lines);

    UserModel user = (UserModel) results.get("user");
    Measure measure = (Measure) results.get("measure");

    if (!userRepo.existsById(user.getRut())) {
      userRepo.save(user);
    } else {
      user = userRepo.getOne(user.getRut());
    }

    if (user.getMeasures() == null) {
      user.setMeasures(new ArrayList<Measure>());
    }

    System.out.println(user.getMeasures().size());

    user.getMeasures().add(measure);
    userRepo.save(user);

    user = userRepo.getOne(user.getRut());
    System.out.println(user.getMeasures().size());

    System.out.println(userRepo.findAll());

    return file.getOriginalFilename();
  }

  public List<Measure> getMeasuresByUser(String user, String fileType)
      throws UserNotFoundException {
    if (fileType != null && !fileService.getFileTypes().contains(fileType)) {
      throw new IllegalArgumentException("The fileType " + fileType + " is not supported");
    }

    if (!userRepo.existsById(user)) {
      throw new UserNotFoundException("The user " + user + " was not found!");
    }

    System.out.println(fileType);
    if (fileType != null) {
      return measureRepo.findByRutAndFileType(user, fileType);
    }

    return userRepo.getOne(user).getMeasures();
  }

  private void printArray(String... strings) {
    for (int i = 0; i < strings.length; i++) {
      System.out.println("index " + i + " - " + strings[i]);
    }
  }

}
