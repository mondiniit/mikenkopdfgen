package cl.mondiniit.mikenko.service.strategy;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.service.FileService;
import java.util.HashMap;
import java.util.Map;

public class PerfilBioquimicoReader implements Reader {

  public Map<String, Object> read(String[] lines) {
    String rut = lines[0].trim();
    String edad = lines[1];
    String sexo = lines[2];
    String nombre = lines[21].replace("Nombre ", "");

//		UserModel userModel = new UserModel(rut, nombre, edad, null, null);

    String procedencia = lines[23];
    String medico = lines[24];
    String numeroOrden = lines[22];

    String glicemia = lines[35].split(" ")[0];
    String nitrogenoUreico = lines[36].split(" ")[0];
    String uremia = lines[37].split(" ")[0];
    String colesterol = lines[38].split(" ")[0];
    String acidoUrico = lines[39].split(" ")[0];
    String proteinasTotales = lines[40].split(" ")[0];
    String albumina = lines[41].split(" ")[0];
    String calcio = lines[42].split(" ")[0];
    String fosforo = lines[43].split(" ")[0];
    String bilirrubina = lines[44].split(" ")[0];
    String fosfatasaAlcalina = lines[45].split(" ")[0];
    String deshidrogenasaLactica = lines[46].split(" ")[0];
    String transaminasa = lines[47].split(" ")[0];

    Map<String, Object> result = new HashMap<String, Object>();

    UserModel userModel = new UserModel(rut, nombre, edad, null, null);
    Measure measure = new Measure();

    measure.setFileType(FileService.PERFIL_BIOQUIMICO);
    measure.setRut(rut);

    result.put("user", userModel);
    result.put("measure", measure);

    measure.setMedico(medico);
    measure.setNumeroOrden(numeroOrden);
    measure.setGlicemia(glicemia);
    measure.setNitrogenoUrico(nitrogenoUreico);
    measure.setUremia(uremia);
    measure.setColesterol(colesterol);
    measure.setAcidoUrico(acidoUrico);
    measure.setProteinasTotales(proteinasTotales);
    measure.setAlbumina(albumina);
    measure.setCalcio(calcio);
    measure.setFosforo(fosforo);
    measure.setBilirrubina(bilirrubina);
    measure.setFosfatasa(fosfatasaAlcalina);
    measure.setDeshidrogenasaLactica(deshidrogenasaLactica);
    measure.setTransaminasa(transaminasa);
    measure.setRut(rut);

    return result;

  }
}
