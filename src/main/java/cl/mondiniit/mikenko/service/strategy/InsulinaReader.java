package cl.mondiniit.mikenko.service.strategy;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.service.FileService;
import java.util.HashMap;
import java.util.Map;

public class InsulinaReader implements Reader {

  public Map<String, Object> read(String[] lines) {
    // TODO Auto-generated method stub
    String nombre = lines[2].split("Informe:")[0];
    String rut = lines[3].split("Edad:")[0].replace("RUT: ", "").trim();
    String edad = lines[3].split(" ")[3];
    String sexo = lines[3].split(" ")[4];
    String fono = lines[4].replace("Fono: ", "");
    String medico = lines[5];
    String ingreso = lines[1].replace("Ingreso:", "");
    String informe = lines[2].split("Informe:")[1];
    String numeroOrden = lines[3].split(" ")[6];

    String glucosa = lines[9].split(" ")[1];
    String insulina = lines[10].split(" ")[1];
    String resistenciaInsulina = lines[11].split(" ")[4];

    Map<String, Object> result = new HashMap<String, Object>();

    UserModel userModel = new UserModel(rut, nombre, edad, null, null);
    Measure measure = new Measure();
    measure.setMedico(medico);
    measure.setIngreso(ingreso);
    measure.setInforme(informe);
    measure.setNumeroOrden(numeroOrden);
    measure.setGlucosa(glucosa);
    measure.setInsulina(insulina);
    measure.setResistenciaInsulina(resistenciaInsulina);

    measure.setFileType(FileService.INSULINA);
    measure.setRut(rut);

    result.put("user", userModel);
    result.put("measure", measure);

    return result;
  }
}
