package cl.mondiniit.mikenko.service.strategy;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.service.FileService;
import java.util.HashMap;
import java.util.Map;

public class VitaminaReader implements Reader {

  public Map<String, Object> read(String[] lines) {
    String nombre = lines[2].split("Informe")[0];
    String rut = lines[3].split("Edad")[0].replace("RUT: ", "").trim();
    String edad = lines[3].split(" ")[3];
    String fono = lines[4];
    String medico = lines[5];
    String sexo = lines[3].split(" ")[4];
    String ingreso = lines[1];
    String informe = lines[2];
    String numeroOrden = lines[3];

    String vitamina = lines[8];

    Map<String, Object> result = new HashMap<String, Object>();

    UserModel userModel = new UserModel(rut, nombre, edad, null, null);
    Measure measure = new Measure();

    result.put("user", userModel);
    result.put("measure", measure);

    measure.setFileType(FileService.VITAMINA);
    measure.setRut(rut);

    measure.setMedico(medico);
    measure.setInforme(informe);
    measure.setIngreso(ingreso);
    measure.setNumeroOrden(numeroOrden);
    measure.setVitamina(vitamina);

    return result;
  }
}
