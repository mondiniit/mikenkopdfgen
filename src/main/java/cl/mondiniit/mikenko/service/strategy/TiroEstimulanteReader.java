package cl.mondiniit.mikenko.service.strategy;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.service.FileService;
import java.util.HashMap;
import java.util.Map;

public class TiroEstimulanteReader implements Reader {

  public Map<String, Object> read(String[] lines) {

    Map<String, Object> result = new HashMap<String, Object>();

    String nombre = lines[2].split("Informe:")[0];
    String rut = lines[3].split("Edad:")[0].replace("RUT: ", "");
    String edad = lines[3].split(" ")[3];
    String sexo = lines[3].split(" ")[4];
    String fono = lines[4].replace("Fono: ", "");
    String medico = lines[5];
    String ingreso = lines[1].replace("Ingreso:", "");
    String informe = lines[2].split("Informe:")[1];
    String numeroOrden = lines[3].split(" ")[6];

    UserModel userModel = new UserModel(rut, nombre, edad, null, null);

    String tiroEstimulante = lines[8].split(" ")[2];
    String t4Libre = lines[9].split(" ")[3];

    Measure measure = new Measure();
    measure.setMedico(medico);
    measure.setIngreso(ingreso);
    measure.setInforme(informe);
    measure.setNumeroOrden(numeroOrden);
    measure.setTiroEstimulante(tiroEstimulante);
    measure.setT4Libre(t4Libre);

    measure.setFileType(FileService.TIRO_ESTIMULANTE);
    measure.setRut(rut);

    result.put("user", userModel);
    result.put("measure", measure);

    return result;
  }
}
