package cl.mondiniit.mikenko.service.strategy;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.service.FileService;
import java.util.HashMap;
import java.util.Map;

public class PerfilLipidicoReader implements Reader{

  public Map<String, Object> read(String[] lines) {
    String nombre = lines[21].replace(":Nombre ", "");
    String rut = lines[0].trim();
    String medico = lines[24];
    String procedencia = lines[23];
    String sexo = lines[2];
    String edad = lines[1];
    String fechaNacimiento = lines[25];
    String numeroOrden = lines[22];
    String tomaMuestra = lines[4];
    String recepcion = lines[3];
    String impresion = lines[26];

    String colesteroltotal = lines[35].split(" ")[0];
    String colesterolHdl = lines[36].split(" ")[0];
    String colesterolVldl = lines[37].split(" ")[0];
    String colesterolLdl = lines[38].split(" ")[0];
    String trigliceridos = lines[39].split(" ")[0];
    String factorDeRiesgoCardiovascular = lines[40].split(" ")[0].replace("FACTOR", "");

    String metodo = lines[41];
    String tecnologia = lines[42];
    String tipoMuestra = lines[43];

    Map<String, Object> result = new HashMap<String, Object>();

    UserModel userModel = new UserModel(rut, nombre, edad, null, null);
    Measure measure = new Measure();

    measure.setFileType(FileService.PERFIL_LIPIDICO);
    measure.setRut(rut);

    measure.setMedico(medico);
    measure.setNumeroOrden(numeroOrden);

    result.put("user", userModel);
    result.put("measure", measure);

    measure.setNumeroOrden(numeroOrden);
    measure.setColesterolTotal(colesteroltotal);
    measure.setColesterolHdl(colesterolHdl);
    measure.setColesterolVldl(colesterolVldl);
    measure.setColesterolHdl(colesterolHdl);
    measure.setTrigliceridos(trigliceridos);
    measure.setFactorDeRiesgoCardiovascular(factorDeRiesgoCardiovascular);
    measure.setTecnologiaTipoMuestra(tecnologia);

    return result;

  }
}
