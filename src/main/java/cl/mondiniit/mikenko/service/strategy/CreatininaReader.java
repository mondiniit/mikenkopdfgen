package cl.mondiniit.mikenko.service.strategy;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.service.FileService;
import java.util.HashMap;
import java.util.Map;

public class CreatininaReader implements Reader {

  public Map<String, Object> read(String[] lines) {

    Map<String, Object> result = new HashMap<String, Object>();

    String nombre = lines[2].split("Informe:")[0];
    String rut = lines[3].split("Edad:")[0].replace("RUT: ", "").trim();
    String edad = lines[3].split(" ")[3];
    String sexo = lines[3].split(" ")[4];
    String fono = lines[4].replace("Fono: ", "");
    String medico = lines[5];
    String ingreso = lines[1].replace("Ingreso:", "");
    String informe = lines[2].split("Informe:")[1];
    String numeroOrden = lines[3].split(" ")[6];

    UserModel userModel = new UserModel(rut, nombre, edad, null, null);
    result.put("user", userModel);

    String creatinina = lines[8].split(" ")[1];
    String tasaDeFiltracionGlomerularEstimada = lines[11].split(" ")[3];
    String valorDeReferenciaTFGEstimada = lines[15].split(" ")[9];

    Measure measure = new Measure();

    measure.setNombre(nombre);
    measure.setRut(rut);
    measure.setEdad(edad);
    measure.setSexo(sexo);
    measure.setFono(fono);
    measure.setMedico(medico);
    measure.setIngreso(ingreso);
    measure.setInforme(informe);
    measure.setNumeroOrden(numeroOrden);

    measure.setCreatinina(creatinina);
    measure.setTasaDeFiltracionGlomedularEstimada(tasaDeFiltracionGlomerularEstimada);
    measure.setValorDeRefereniaTFGEstimada(valorDeReferenciaTFGEstimada);

    measure.setFileType(FileService.CREATININA);
    measure.setRut(rut);

    result.put("measure", measure);

    return result;
  }
}
