package cl.mondiniit.mikenko.service.strategy;

import cl.mondiniit.mikenko.model.Measure;
import cl.mondiniit.mikenko.model.UserModel;
import cl.mondiniit.mikenko.service.FileService;
import java.util.HashMap;
import java.util.Map;

public class ElectrolitosReader implements Reader {

  public Map<String, Object> read(String[] lines) {
    // TODO Auto-generated measdasthod stub
    String nombre = lines[2].split("Informe:")[0];
    String rut = lines[3].split("Edad:")[0].replace("RUT: ", "").trim();
    String edad = lines[3].split(" ")[3];
    String sexo = lines[3].split(" ")[4];
    String fono = lines[4].replace("Fono: ", "");
    String medico = lines[5];
    String ingreso = lines[1].replace("Ingreso:", "");
    String informe = lines[2].split("Informe:")[1];
    String numeroOrden = lines[3].split(" ")[6];

    String sodio = lines[9].split(" ")[1];
    String potasio = lines[10].split(" ")[1];
    String cloro = lines[11].split(" ")[1];

    Map<String, Object> result = new HashMap<String, Object>();

    UserModel userModel = new UserModel(rut, nombre, edad, null, null);
    Measure measure = new Measure();

    result.put("user", userModel);
    result.put("measure", measure);

    measure.setFileType(FileService.ELECTROLITOS);
    measure.setRut(rut);

    measure.setMedico(medico);
    measure.setIngreso(ingreso);
    measure.setInforme(informe);
    measure.setNumeroOrden(numeroOrden);
    measure.setSodio(sodio);
    measure.setPotasio(potasio);
    measure.setCloro(cloro);

    return result;

  }
}
