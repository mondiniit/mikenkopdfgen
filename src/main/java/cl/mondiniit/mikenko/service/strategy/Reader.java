package cl.mondiniit.mikenko.service.strategy;

import java.util.Map;

public interface Reader {

  Map<String, Object> read(String[] lines);

}
