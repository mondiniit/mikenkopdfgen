package cl.mondiniit.mikenko.service;

import cl.mondiniit.mikenko.service.strategy.CreatininaReader;
import cl.mondiniit.mikenko.service.strategy.ElectrolitosReader;
import cl.mondiniit.mikenko.service.strategy.HemoglobinaReader;
import cl.mondiniit.mikenko.service.strategy.InsulinaReader;
import cl.mondiniit.mikenko.service.strategy.PerfilBioquimicoReader;
import cl.mondiniit.mikenko.service.strategy.PerfilLipidicoReader;
import cl.mondiniit.mikenko.service.strategy.Reader;
import cl.mondiniit.mikenko.service.strategy.TiroEstimulanteReader;
import cl.mondiniit.mikenko.service.strategy.VitaminaReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import java.util.Map;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {

  public static final String CREATININA = "Creatinina";
  public static final String TIRO_ESTIMULANTE = "Tiro Estimulante";
  public static final String HEMOGLOBINA = "Hemoglobina";
  public static final String INSULINA = "Insulina";
  public static final String ELECTROLITOS = "Electrolitos";
  public static final String VITAMINA = "Vitamina";
  public static final String PERFIL_LIPIDICO = "Perfil Lipidico";
  public static final String PERFIL_BIOQUIMICO = "Perfil Bioquimico";

  private Reader reader;

  public List<String> getFileTypes() {
    return Arrays
        .asList(CREATININA, TIRO_ESTIMULANTE, HEMOGLOBINA, INSULINA, ELECTROLITOS, VITAMINA,
            PERFIL_LIPIDICO, PERFIL_BIOQUIMICO);
  }

  public String[] getLines(MultipartFile file) throws IOException {
    try (PDDocument document = PDDocument.load(file.getBytes())) {

      document.getClass();

      if (document.isEncrypted()) {
        throw new IllegalArgumentException("The document is encrypted");
      }

      PDFTextStripperByArea stripper = new PDFTextStripperByArea();
      stripper.setSortByPosition(true);

      PDFTextStripper tStripper = new PDFTextStripper();

      String pdfFileInText = tStripper.getText(document);
      // System.out.println("Text:" + st);

      // split by whitespace
      String lines[] = pdfFileInText.split("\\r?\\n");
      System.out.println(lines.length);

      for (int i = 0; i < lines.length; i++) {
        System.out.println(i + " " + lines[i]);
      }

      return lines;
    }
  }

  public Map<String, Object> readByFileType(String fileType, String[] lines) {
    Map<String, Object> results = null;
    switch (fileType) {
      case CREATININA:
        reader = new CreatininaReader();
        break;
      case TIRO_ESTIMULANTE:
        reader = new TiroEstimulanteReader();
        break;
      case HEMOGLOBINA:
        reader = new HemoglobinaReader();
        break;
      case INSULINA:
        reader = new InsulinaReader();
        break;
      case ELECTROLITOS:
        reader = new ElectrolitosReader();
        break;
      case VITAMINA:
        reader = new VitaminaReader();
        break;
      case PERFIL_LIPIDICO:
        reader = new PerfilLipidicoReader();
        break;
      case PERFIL_BIOQUIMICO:
        reader = new PerfilBioquimicoReader();
        break;
    }
    results = reader.read(lines);

    return results;
  }

}
