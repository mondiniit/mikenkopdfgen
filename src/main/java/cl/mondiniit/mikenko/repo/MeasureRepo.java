package cl.mondiniit.mikenko.repo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.mondiniit.mikenko.model.Measure;

public interface MeasureRepo extends JpaRepository<Measure, BigDecimal> {

	List<Measure> findByRut(String rut);
	List<Measure> findByRutAndFileType(String rut,String fileType);
	
}
