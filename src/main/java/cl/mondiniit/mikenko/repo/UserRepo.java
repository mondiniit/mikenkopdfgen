package cl.mondiniit.mikenko.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.mondiniit.mikenko.model.UserModel;

@Repository
public interface UserRepo extends JpaRepository<UserModel, String> {

}
