package cl.mondiniit.mikenko.to;

public class ResponseTO {

	private int code;
	private String message;
	private Object response;

	private ResponseTO() {
	}

	private ResponseTO(int code, String message, Object response) {
		super();
		this.code = code;
		this.message = message;
		this.response = response;
	}

	public static ResponseTO getDefault() {
		return new ResponseTO(-10, "default", null);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
}
