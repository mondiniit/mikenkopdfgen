
FROM openjdk:8-jre-alpine3.9
COPY target/mikenko-0.0.1-SNAPSHOT.jar  /mikenko.jar

# set the startup command to execute the jar
CMD ["java", "-jar", "/mikenko.jar"]
